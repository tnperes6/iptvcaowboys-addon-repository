# -*- coding: UTF-8 -*-
"""
    Copyright (C) 2014  smokdpi

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


""" Site information used for main menu if more than 1 site """
title = 'PlayThis'
image = ''
art = ''
order = 1


class Site():

    def __init__(self, params):
        from addon import Addon

        a = Addon()
        site = self.__module__
        mode = params['mode']

        if mode == 'main':
            toplay = a.play_input(site)
            if toplay:
                from playback import Playback
                Playback().play_this(toplay)
            else:
                a.common.container_refresh()
#!/usr/bin/python
#coding=utf-8

import xbmc,xbmcaddon,xbmcplugin,xbmcgui,sys,urllib,urllib2,re,os,base64

addonID = 'plugin.video.newlivetv'
addon = xbmcaddon.Addon(addonID)
pluginhandle = int(sys.argv[1])
home = xbmc.translatePath(xbmcaddon.Addon().getAddonInfo('path') ).decode("utf-8")
logos = xbmc.translatePath(os.path.join(home, 'resources', 'logos'))
dataPath = xbmc.translatePath(os.path.join(home, 'resources', 'data'))

def TVChannel(url):
    print url
    xmlcontent = GetUrl(url)
    names = re.compile('<name>(.+?)</name>').findall(xmlcontent)
    if len(names) == 1:
        items = re.compile('<item>(.+?)</item>').findall(xmlcontent)
        for item in items:
            thumb=""
            title=""
            link=""
            if "/title" in item:
                title = re.compile('<title>(.+?)</title>').findall(item)[0]
            if "/link" in item:
                link = re.compile('<link>(.+?)</link>').findall(item)[0]			
            if "/thumbnail" in item:
                thumb = re.compile('<thumbnail>(.+?)</thumbnail>').findall(item)[0]
            if "com/host/" in thumb:
                addLink(title, link, logos+'/'+thumb.split("/")[-1])			
            else:			
                addLink(title, link, thumb)
        xbmc.executebuiltin('Container.SetViewMode(52)')		
    else:
        names = re.compile('<name>(.+?)</name>\s*<thumbnail>(.+?)</thumbnail>').findall(xmlcontent)	
        for name, thumb in names:			
           addDir(name, url+"?n="+name, 'index', thumb)
        xbmc.executebuiltin('Container.SetViewMode(52)')
		
def Channel():
    if os.path.exists(dataPath)==True:
		content = extract(edata,(open(dataPath+'/'+'oO000oo00o0o.xml').read()))
    else:
	    content = GetUrl(DecryptData(homeurl))
    match=re.compile("<title>([^<]*)<\/title>\s*<link>([^<]+)<\/link>\s*<thumbnail>(.+?)</thumbnail>").findall(content)	
    for title,url,thumbnail in match:
        if os.path.exists(dataPath)==True:
		    addDir(title,dataPath+'/'+url.split("/")[-1],'tvchannel',logos+'/'+thumbnail.split("/")[-1])
        else:
		    addDir(title,url,'tvchannel',thumbnail)		
    xbmc.executebuiltin('Container.SetViewMode(%d)' % 500)	
	
def resolveUrl(url):
	item=xbmcgui.ListItem(path=url)
	xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, item)	  
	return	

def extract(k, e):
  dec = []
  e = base64.urlsafe_b64decode(e)
  for i in range(len(e)):
      kc = k[i % len(k)]
      d = chr((256 + ord(e[i]) - ord(kc)) % 256)
      dec.append(d)
  return "".join(dec)
	  	  
def Index(url):
    byname = url.split("?n=")[1]
    url = url.split("?")[0]	
    xmlcontent = GetUrl(url)
    channels = re.compile('<channel>(.+?)</channel>').findall(xmlcontent)
    for channel in channels:
        if byname in channel:
            items = re.compile('<item>(.+?)</item>').findall(channel)
            for item in items:
                thumb=""
                title=""
                link=""
                if "/title" in item:
                    title = re.compile('<title>(.+?)</title>').findall(item)[0]
                if "/link" in item:
                    link = re.compile('<link>(.+?)</link>').findall(item)[0]
                if "/thumbnail" in item:
                    thumb = re.compile('<thumbnail>(.+?)</thumbnail>').findall(item)[0]
                if "youtube" in link:					
                    addDir(title, link, 'episodes', thumb)
                else:			
                    addLink(title, link, thumb)					
    skin_used = xbmc.getSkinDir()
    if skin_used == 'skin.xeebo':
        xbmc.executebuiltin('Container.SetViewMode(50)')

def GetUrl(url):
    link = ""
    if os.path.exists(url)==True:
		link = extract(edata, (open(url).read()))
    else:
        print url
        req = urllib2.Request(url)
        req.add_header('User-Agent', 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)')
        response = urllib2.urlopen(req)
        link=response.read()
        response.close()
    link = ''.join(link.splitlines()).replace('\'','"')
    link = link.replace('\n','')
    link = link.replace('\t','')
    link = re.sub('  +',' ',link)
    link = link.replace('> <','><')
    return link

def addDir(name,url,mode,iconimage):
    u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)
    ok=True
    liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": name } )
    if ('www.youtube.com/user/' in url) or ('www.youtube.com/channel/' in url):
        u = 'plugin://plugin.video.youtube/%s/%s/' % (url.split( '/' )[-2], url.split( '/' )[-1])
    ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
    return ok	
	
def addLink(name,url,iconimage):
    u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode=stream"+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)	
    ok=True
    liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
    liz.setInfo( type="Video", infoLabels={ "Title": name } )
    liz.setProperty('IsPlayable', 'true')
    if url.startswith('plugin'):	
        u = url	
        return xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)		
    ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz)  
    return ok
			
def parameters_string_to_dict(parameters):
    paramDict = {}
    if parameters:
        paramPairs = parameters[1:].split("&")
        for paramsPair in paramPairs:
            paramSplits = paramsPair.split('=')
            if (len(paramSplits)) == 2:
                paramDict[paramSplits[0]] = paramSplits[1]
    return paramDict

DecryptData = base64.b64decode	
homeurl = 'aHR0cHM6Ly9nb29nbGVkcml2ZS5jb20vaG9zdC8wQjd6a2tRd281cHI1ZmpOVWNIY3dNRWt5UzBneFVHUnZRbE16YUdweFNEUXRaR050VVZaclN5MWZRemhEU2pKMU5FaG1ibU0vdjExLnhtbA=='
edata = 'aHR0cHM6Ly9nb29nbGVkcml2ZS5jb20vaG9zdC8wQjd6a2tRd281cHI1ZmpOVWNIY3dNRWt5UzBn\
eFVHUnZRbE16YUdweFNEUXRaR050VVZaclN5MWZRemhEU2pKMU5FaG1ibU0vaG9tZTIueG1s'
params=parameters_string_to_dict(sys.argv[2])
mode=params.get('mode')
url=params.get('url')
name=params.get('name')
iconimage=None

try:
  iconimage=urllib.unquote_plus(params["iconimage"])
except: pass

if type(url)==type(str()):
    url=urllib.unquote_plus(url)
sysarg=str(sys.argv[1])

if mode == 'tvchannel':TVChannel(url)
elif mode == 'index':Index(url)
	
elif mode=='stream':
    dp = xbmcgui.DialogProgress()
    dp.create('Brought to you by NEW LIVE-TV', 'Loading video. Please wait...')
    xbmc.sleep(256)	
    resolveUrl(url)
    dp.close()
    del dp	
	
else: Channel()

xbmcplugin.endOfDirectory(int(sysarg))
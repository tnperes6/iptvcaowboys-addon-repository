version=1
title=Navi-X Home
#
type=playlist
name=Navi-Xtreme Media Portal
description=Visit http://www.navixtreme.com./description
thumb=http://scrape.navixtreme.com/images/logos/logo-xbmchub.png
URL=http://www.navixtreme.com/playlist/50242/navi-xtreme_nxportal_home.plx
#
type=playlist
name=Browse History
URL=history.plx
date=2009-10-03
#
type=playlist
name=My Favorites
URL=favorites.plx
#
type=directory
name=My Playlists (local disk)
URL=My Playlists
#
type=window
name=My Videos
URL=video
#
type=window
name=My Music
URL=music
#
type=window
name=My Pictures
URL=pictures
#
type=window
name=File Manager
URL=filemanager
#
type=playlist
name=Navi-X updates (Script Navi-X 3.7.8)
URL=http://navi-x.googlecode.com/svn/trunk/Playlists/scripts.plx
date=2013-05-09
#
type=playlist
name=About Navi-X
URL=http://navi-x.googlecode.com/svn/trunk/Playlists/about.plx
#

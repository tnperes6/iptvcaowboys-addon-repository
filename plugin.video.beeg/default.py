__scriptname__ = "beeg.com"
__author__ = "achilles"
__url__ = "https://code.google.com/p/achilles-projects/"
__scriptid__ = "plugin.video.beeg"
__credits__ = "Anyone that sees any of their code in here..."
__version__ = "1.0.0"

import urllib,urllib2,re
import xbmc,xbmcplugin,xbmcgui,sys

homepage = 'http://beeg.com'

def title_case(line):
  return ' '.join([s[0].upper() + s[1:] for s in line.split(' ')])

def GET_HTML(url):
  req = urllib2.Request(url)
  req.add_header('User-Agent','Mozilla/5.0 (Windows NT 5.1; rv:8.0) Gecko/20100101 Firefox/8.0')
  req.add_header('Referer', homepage)
  response = urllib2.urlopen(req)
  html=response.read()
  response.close()
  return html

def LIST_SECTIONS():
  html = GET_HTML(homepage)
  match=re.compile('<li><a target="_self" href="(.+?)" >(.+?)</a></li>', re.DOTALL | re.MULTILINE).findall(html)
  for cat_url,cat_title in sorted(match):
    title = title_case(cat_title)
    url = homepage + cat_url
    addDir(title,url,2,'')
  xbmcplugin.endOfDirectory(int(sys.argv[1]))

def LIST_ITEMS(url):
  html = GET_HTML(url)
  ids = re.search('var tumbid  =(.+?);', html).group(1)
  descs = re.search('var tumbalt =(.+?)\];', html).group(1)

  ids = ids[1:-1]
  idlist = ids.split(',')
  descs = descs[2:-2]
  descs = descs.decode('string_escape')
  desclist =  descs.split("','")

  for id,desc in zip(idlist, desclist):
    vid_title = desc
    vid_url = 'http://beeg.com/' + id
    vid_image = 'http://cdn.anythumb.com/236x177/' + id + '.jpg'
    addDownLink(vid_title,vid_url,3,vid_image)
  try:
    next = re.search('<a href="(.+?)" target="_self" id="paging_next">', html).group(1)
    addDir(' Next Page',next,2,'')
  except:
    pass
  xbmcplugin.endOfDirectory(int(sys.argv[1]))

def HOME():
  home = 'http://beeg.com/section/home/'
  addDir('  Categories->','http://beeg.com/',1,'')
  LIST_ITEMS(home)
  xbmcplugin.endOfDirectory(int(sys.argv[1]))

def VIDEOLINKS(url,title):
  html = GET_HTML(url)
  link = re.search("'file': '(.+?)'", html).group(1)
  print 'Link: ' + link
  listitem = xbmcgui.ListItem(title)
  listitem.setInfo('video', {'Title': title})
  xbmc.Player().play( str(link), listitem)

def get_params():
  param=[]
  paramstring=sys.argv[2]
  if len(paramstring)>=2:
    params=sys.argv[2]
    cleanedparams=params.replace('?','')
    if (params[len(params)-1]=='/'):
      params=params[0:len(params)-2]
    pairsofparams=cleanedparams.split('&')
    param={}
    for i in range(len(pairsofparams)):
      splitparams={}
      splitparams=pairsofparams[i].split('=')
      if (len(splitparams))==2:
        param[splitparams[0]]=splitparams[1]
  return param

def addDir(name,url,mode,iconimage):
  u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
  ok=True
  liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png",thumbnailImage=iconimage)
  liz.setInfo( type="Video", infoLabels={ "Title": name })
  ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
  return ok

def addDownLink(name,url,mode,iconimage):
  u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
  ok=True
  liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
  liz.setInfo( type="Video", infoLabels={ "Title": name } )

  ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False)
  return ok

params=get_params()
url=None
name=None
mode=None

try:
  url=urllib.unquote_plus(params["url"])
except:
  pass
try:
  name=urllib.unquote_plus(params["name"])
except:
  pass
try:
  mode=int(params["mode"])
except:
  pass

print 'Mode: ' + str(mode)
print 'URL : ' + str(url)
print 'Name: ' + str(name)

if mode==None or url==None or len(url)<1:
  HOME()
elif mode==1:
  LIST_SECTIONS()
elif mode==2:
  LIST_ITEMS(url)
elif mode==3:
  VIDEOLINKS(url,name)

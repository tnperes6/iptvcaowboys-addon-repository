# -*- coding: utf-8 -*-

'''
Copyright (C) 2015                                                     

This program is free software: you can redistribute it and/or modify   
it under the terms of the GNU General Public License as published by   
the Free Software Foundation, either version 3 of the License, or      
(at your option) any later version.                                    

This program is distributed in the hope that it will be useful,        
but WITHOUT ANY WARRANTY; without even the implied warranty of         
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          
GNU General Public License for more details.                           

You should have received a copy of the GNU General Public License      
along with this program. If not, see <http://www.gnu.org/licenses/>  
'''                                                                           

import urllib, urllib2, re, os, sys, base64, time
import xbmc, xbmcplugin, xbmcgui, xbmcaddon

mysettings = xbmcaddon.Addon(id = 'plugin.video.netsuperaddon')
profile = mysettings.getAddonInfo('profile')
home = mysettings.getAddonInfo('path')
fanart = xbmc.translatePath(os.path.join(home, 'fanart.jpg'))
icon = xbmc.translatePath(os.path.join(home, 'icon.png'))
logos = xbmc.translatePath(os.path.join(home, 'resources', 'logos\\'))
playlists = xbmc.translatePath(os.path.join(home, 'resources', 'playlists/'))
ImgNote = xbmc.translatePath(os.path.join(home, "imgNote.jpg"))
set_cherry = mysettings.getSetting('cherry')
set_ListOfAddons = mysettings.getSetting('ListOfAddons')
set_kodi4viet = mysettings.getSetting('kodi4viet')
master_password = mysettings.getSetting('master_password')
password = mysettings.getSetting('password')
passcode = 'UG9wcGVy'    
date_code = time.strftime('%m%d%Y')	
channel_name = '<name>(.*?)</name>\s*<thumbnail>(.*?)</thumbnail>'
xml_regex = '<title>(.*?)</title>\s*<link>(.*?)</link>\s*<thumbnail>(.*?)</thumbnail>'

def read_file(file):
	try:
		f = open(file, 'r')
		content = f.read()
		f.close()
		return content	
	except:
		pass 

def thumb_view():
	xbmc.executebuiltin('Container.SetViewMode(%d)' % 500)	

def notifyByImg():
	imgloc = xbmcgui.ControlImage(0, 0, 1280 ,720, ImgNote)
	img = xbmcgui.WindowDialog()
	img.addControl(imgloc)
	img.doModal()
	
def main():
	if os.path.exists(ImgNote):
		notifyByImg()
		os.remove(ImgNote)
	content = read_file(playlists + 'mainmenu.xml')
	match = re.compile(xml_regex + '\s*<mode>(.*?)</mode>').findall(content)
	for name, url, thumb, add_mode in match:
		if 'YouTube Add-on' in name: 
			add_link(name, url, '', logos + thumb, fanart)
		else: 
			if set_cherry == 'true':
				add_dir(name, 'category', add_mode, logos + thumb, fanart)
			else:
				if 'Cherry' in name: 
					pass
				else: 
					add_dir(name, 'category', add_mode, logos + thumb, fanart)
	thumb_view()	
		
def viet():
	content = read_file(playlists + 'viet.xml')
	match = re.compile(channel_name).findall(content)
	for name, thumb in match:
		if 'Redirect2MediaList4SingleAddon' not in name:
			add_dir(name, 'channelname', 2, logos + thumb, fanart)
		else:
			viet_channel_list(name)
	thumb_view()
			
def viet_channel_list(name):
	name = name.replace('[', '\[').replace(']', '\]')
	content = read_file(playlists + 'viet.xml')
	match = re.compile('<channel>\s*<name>' + name + '</name>((?s).+?)</channel>').findall(content)
	for content in match:
		index(name, content)
	
def other_channel(name):
	if 'Other' in name:
		content = read_file(playlists + 'other.xml')
		index(name, content)
	elif 'Cherry' in name:
		content = read_file(playlists + 'adult.xml')	
		index(name, content)

def cherry(name):
	if master_password != MyCode(passcode):	
		if len(password) < 1:
			mysettings.openSettings()
			sys.exit()
		else:
			try:
				keyb = xbmc.Keyboard('', 'Enter password')
				keyb.doModal()
				if (keyb.isConfirmed()):
					user_input = urllib.quote_plus(keyb.getText())
				#if user_input != (date_code + password):				# Rolling code by date (mmddyyyy - 08272015)	
				if user_input != (MyCode(passcode) + password):		
					sys.exit()
				other_channel(name)	
			except:
				sys.exit()
	else:			
		other_channel(name)
		
def index(name, content):
	match = re.compile(xml_regex).findall(content)
	for title, url, thumb in match:
		add_link(title, url, '', logos + thumb, fanart)
	thumb_view()

def get_params():
	param = []
	paramstring = sys.argv[2]
	if len(paramstring)>= 2:
		params = sys.argv[2]
		cleanedparams = params.replace('?', '')
		if (params[len(params)-1] == '/'):
			params = params[0:len(params)-2]
		pairsofparams = cleanedparams.split('&')
		param = {}
		for i in range(len(pairsofparams)):
			splitparams = {}
			splitparams = pairsofparams[i].split('=')
			if (len(splitparams)) == 2:
				param[splitparams[0]] = splitparams[1]
	return param

def add_dir(name, url, mode, iconimage, fanart):
	u = sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + "&name=" + urllib.quote_plus(name) + "&iconimage=" + urllib.quote_plus(iconimage)
	ok = True
	liz = xbmcgui.ListItem(name, iconImage = "DefaultFolder.png", thumbnailImage = iconimage)
	liz.setInfo( type = "Video", infoLabels = { "Title": name } )
	liz.setProperty('fanart_image', fanart)
	ok = xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = u, listitem = liz, isFolder = True)
	return ok

def add_link(name, url, mode, iconimage, fanart):
	u = sys.argv[0] + "?url=" + urllib.quote_plus(url) + "&mode=" + str(mode) + "&name=" + urllib.quote_plus(name) + "&iconimage=" + urllib.quote_plus(iconimage)
	ok = True
	liz = xbmcgui.ListItem(name, iconImage = "DefaultFolder.png", thumbnailImage = iconimage)
	liz.setInfo( type = "Video", infoLabels = { "Title": name } )
	liz.setProperty('fanart_image', fanart)
	ok = xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = url, listitem = liz, isFolder = True)	
	return ok

def get_link(name, url, mode, iconimage, fanart):
	liz = xbmcgui.ListItem(name, iconImage = "DefaultVideo.png", thumbnailImage = iconimage)
	liz.setInfo( type = "Video", infoLabels = { "Title": name } )
	liz.setProperty('fanart_image', fanart)
	liz.setProperty('IsPlayable', 'true')  
	xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url = url, listitem = liz)  
	
params = get_params()
MyCode = base64.b64decode
url = None
name = None
mode = None
iconimage = None

try:
	url = urllib.unquote_plus(params["url"])
except:
	pass
try:
	name = urllib.unquote_plus(params["name"])
except:
	pass
try:
	mode = int(params["mode"])
except:
	pass	
try:
	iconimage = urllib.unquote_plus(params["iconimage"])
except:
	pass  

print "Mode: " + str(mode)
print "URL: " + str(url)
print "Name: " + str(name)
print "iconimage: " + str(iconimage)

if mode == None or url == None or len(url) < 1:
	main()

if mode == 1:
	viet()

if mode == 2:
	viet_channel_list(name)
	
if mode == 3:
	cherry(name)

if mode == 4:
	other_channel(name)
		
xbmcplugin.endOfDirectory(int(sys.argv[1]))
import xbmcplugin,xbmcgui,xbmc,xbmcaddon,os,sys
AddonID="pvr.iptvsimple"
AddonName="PVR IPTV Simple Client"
dialog=xbmcgui.Dialog()
xbmc.executebuiltin("UpdateLocalAddons")
xbmc.executebuiltin("UpdateAddonRepos")
choice=dialog.yesno(AddonName+" Add-on Requires Update","This add-on may still be in the process of the updating. We recommend waiting but if you've already tried that and it's not updating you can try re-installing via the CP backup method.",yeslabel="Install Option 2", nolabel="Wait...")
if choice == 1: xbmc.executebuiltin('ActivateWindow(10001,"plugin://plugin.program.totalinstaller/?mode=grab_addons&url=%26redirect%26addonid%3d'+AddonID+'")')
xbmcplugin.endOfDirectory(int(sys.argv[1]))
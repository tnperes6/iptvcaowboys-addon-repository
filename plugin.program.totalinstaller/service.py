import urllib, urllib2, re, xbmcplugin, xbmcgui, xbmc, xbmcaddon, os, sys, time, xbmcvfs
import shutil
import time
from addon.common.addon import Addon

#################################################
AddonID        = 'plugin.program.totalinstaller'
#################################################
dialog         =  xbmcgui.Dialog()
dp             =  xbmcgui.DialogProgress()
ADDON          =  xbmcaddon.Addon(id=AddonID)
ADDONDATA      =  xbmc.translatePath(os.path.join('special://home/userdata/addon_data',''))
internetcheck  =  ADDON.getSetting('internetcheck')
cbnotifycheck  =  ADDON.getSetting('cbnotifycheck')
mynotifycheck  =  ADDON.getSetting('mynotifycheck')
idfile         =  xbmc.translatePath(os.path.join(ADDONDATA,AddonID,'id.xml'))
TBSDATA        =  xbmc.translatePath(os.path.join(ADDONDATA,AddonID,''))
openelec       =  '/storage/openelec_temp'

print'###### Community Portal Update Service ######'

if os.path.exists(openelec):
    dialog.ok('Build Install Success!','Your build has now been installed but to ensure you\'re notified of any future updates please go back to the Community Portal addon and install the guisettings fix for this build.')
    shutil.rmtree(openelec)

if not os.path.exists(TBSDATA):
    os.makedirs(TBSDATA)

if not os.path.exists(idfile):
    localfile = open(idfile, mode='w+')
    localfile.write('id="None"\nname="None"')
    localfile.close()
	
if internetcheck == 'true':
    xbmc.executebuiltin('XBMC.AlarmClock(internetloop,XBMC.RunScript(special://home/addons/'+AddonID+'/connectivity.py,silent=true),00:00:30,silent,loop)')

if mynotifycheck=='true' or cbnotifycheck=='true':
    xbmc.executebuiltin('XBMC.AlarmClock(Notifyloop,XBMC.RunScript(special://home/addons/'+AddonID+'/notify.py,silent=true),00:30:00,silent,loop)')

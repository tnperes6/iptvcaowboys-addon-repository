#!/usr/bin/python
#coding=utf-8

import xbmc, xbmcaddon, xbmcplugin, xbmcgui, sys, urllib, urllib2, re, os, base64

addonID = 'plugin.video.newviettv24'
addon = xbmcaddon.Addon(addonID)
home = xbmc.translatePath(xbmcaddon.Addon().getAddonInfo('path')).decode('utf-8')
icon = xbmc.translatePath(os.path.join(home, 'icon.png'))
folderPath = xbmc.translatePath(os.path.join(home, 'resources', 'playlists'))
homeurl = 'http://www.viettv24.com/main/getStreamingServer.php'
menuFile = folderPath+'/'+'00000001.txt'

def getUrl(url):
	link = ''
	if os.path.exists(url) == True:
		link = open(url).read()
	else:
		req = urllib2.Request(url)
		req.add_header('User-Agent' , 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)')
		response = urllib2.urlopen(req)
		link=response.read()
		response.close()
	link = ''.join(link.splitlines()).replace('\'','"')
	link = link.replace('\n','')
	link = link.replace('\t','')
	link = re.sub('  +',' ',link)
	link = link.replace('> <','><')
	return link

def mainMenu():
	content = getUrl(menuFile)
	for title, url in eval(content):	
		addDir(title, url, 'indexes', icon)

def indexes(url):
	content = ''.join((extract('umbala', open(folderPath+'/'+url).read()).splitlines()))	
	names = re.compile('<name>(.+?)</name>').findall(content)
	if len(names) == 1:
		items = re.compile('<item>(.+?)</item>').findall(content)
		for item in items:		
			title=''
			link=''
			thumb=''			
			if '/title' in item:
				title = re.compile('<title>(.+?)</title>').findall(item)[0]			
			if '/link' in item:
				link = re.compile('<link>(.+?)</link>').findall(item)[0]
			if '/thumbnail' in item:
				thumb = re.compile('<thumbnail>(.+?)</thumbnail>').findall(item)[0]				
			if ('/user/' in link) or ('/channel/' in link):			
				addDir(title, link, 'utube', thumb)				
			else:
				addLink(title, link, 'play', thumb)
	else:
		for name in names:							
			addDir(name, url + '?n=' + name, 'index', '')

def index(url):
	byname = url.split('?n=')[1]
	url = url.split('?')[0]
	content = getUrl(url)
	channels = re.compile('<channel>(.+?)</channel>').findall(content)
	for channel in channels:
		if byname in channel:
			items = re.compile('<item>(.+?)</item>').findall(channel)
			for item in items:
				title=''
				link=''
				thumb=''				
				if '/title' in item:
					title = re.compile('<title>(.+?)</title>').findall(item)[0]
				if '/link' in item:
					link = re.compile('<link>(.+?)</link>').findall(item)[0]
				if '/thumbnail' in item:
					thumb = re.compile('<thumbnail>(.+?)</thumbnail>').findall(item)[0]
				if 'youtube' in link:					
					addDir(title, link, 'utube', icon)
				else:					
					addLink(title, link, 'play', icon)
			
def extract(k, e):
	dec = []
	e = base64.urlsafe_b64decode(e)
	for i in range(len(e)):
		kc = k[i % len(k)]
		d = chr((256 + ord(e[i]) - ord(kc)) % 256)
		dec.append(d)
	return ''.join(dec)
			
def urlResolver(title, url):
	if ('youtube' in url) :
		match = re.compile ('(youtu\.be\/|youtube-nocookie\.com\/|youtube\.com\/(watch\?(.*&)?v=|(embed|v|user)\/))([^\?&"\'>]+)').findall(url)
		url = ('plugin://plugin.video.youtube/play/?video_id=%s' % match[0][len(match[0])-1].replace('v/', ''))
		xbmc.executebuiltin('xbmc.PlayMedia('+url+')')
	else :
		if url.isdigit() or url.isalpha() or url.isalnum(): 
			data = urllib.urlencode({'strname' : '%s' % url})
			url = urllib2.urlopen(homeurl, data).read() 
		title = urllib.unquote_plus(title)
		playlist = xbmc.PlayList(1)
		playlist.clear()
		listitem = xbmcgui.ListItem(title, iconImage='DefaultVideo.png', thumbnailImage=iconimage)
		listitem.setInfo('video', {'Title': title})
		xbmcPlayer = xbmc.Player()
		playlist.add(url, listitem)
		xbmcPlayer.play(playlist)

def getParams(parameters):
	param = {}
	if parameters:
		paramPairs = parameters[1:].split('&')
		for paramsPair in paramPairs:
			paramSplits = paramsPair.split('=')
			if (len(paramSplits)) == 2:
				param[paramSplits[0]] = paramSplits[1]
	return param

def addDir(name, url, mode, iconimage):
	u=sys.argv[0]+'?url='+urllib.quote_plus(url)+'&mode='+str(mode)+'&name='+urllib.quote_plus(name)+'&iconimage='+urllib.quote_plus(iconimage)
	ok=True
	liz=xbmcgui.ListItem(name, iconImage='DefaultFolder.png', thumbnailImage=iconimage)
	liz.setInfo( type='Video', infoLabels={ 'Title': name } )
	if ('www.youtube.com/user/' in url) or ('www.youtube.com/channel/' in url):
		u = 'plugin://plugin.video.youtube/%s/%s/' % (url.split( '/' )[-2], url.split( '/' )[-1])
	ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
	return ok			

def addLink(name, url, mode, iconimage):
	u=sys.argv[0]+'?url='+urllib.quote_plus(url)+'&mode='+str(mode)+'&name='+urllib.quote_plus(name)+'&iconimage='+urllib.quote_plus(iconimage)
	ok=True
	liz=xbmcgui.ListItem(name, iconImage='DefaultVideo.png', thumbnailImage=iconimage)
	liz.setInfo( type='Video', infoLabels={ 'Title': name } )
	ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz)
	return ok

params=getParams(sys.argv[2])
mode=params.get('mode')
url=params.get('url')
name=params.get('name')
iconimage=params.get('iconimage')

try:
	iconimage=urllib.unquote_plus(params['iconimage'])
except:
	pass
	
if type(url)==type(str()):
	url=urllib.unquote_plus(url)

print 'Mode: ' + str(mode)
print 'URL: ' + str(url)
print 'Name: ' + str(name)
print 'iconimage: ' + str(iconimage)
	
if mode == 'indexes':
	indexes(url)
	
elif mode == 'index':
	index(url)
	
elif mode=='play':
	dp = xbmcgui.DialogProgress()
	dp.create('Brought to you by New VietTV24', 'Loading video...')
	xbmc.sleep(500)
	urlResolver(name, url)
	dp.close()
	del dp
	
else:
	mainMenu()
	
xbmcplugin.endOfDirectory(handle=int(sys.argv[1]))
import xbmc

osWin = xbmc.getCondVisibility('system.platform.windows')
osAndroid = xbmc.getCondVisibility('system.platform.android')

if osAndroid:
	xbmc.executebuiltin("StartAndroidActivity(com.netflix.ninja)")			# Amazon Fire TV/Fire Stick	
	xbmc.executebuiltin("StartAndroidActivity(com.netflix.mediaclient)")	# Android

if osWin:	# Chrome Launcher for Win/OSX/Linux
	xbmc.executebuiltin("ActivateWindow(10001, plugin://plugin.program.chrome.launcher/?kiosk=no&mode=showSite&stopPlayback=no&url=http%3a%2f%2fwww.netflix.com)")
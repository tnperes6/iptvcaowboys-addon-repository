import urllib,urllib2,re,sys,xbmcplugin,xbmcgui,xbmcaddon,xbmc,os


import json



PLUGIN='plugin.video.youtubesearch'

ADDON = xbmcaddon.Addon(id=PLUGIN)
home = ADDON.getAddonInfo('path')
downloads= ADDON.getSetting('sfdownloads')
datapath = xbmc.translatePath(ADDON.getAddonInfo('profile'))
art= "%s/art/"%ADDON.getAddonInfo("path")

def OPEN_URL(url):
    req = urllib2.Request(url)
    req.add_header('User-Agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3')
    response = urllib2.urlopen(req)
    link=response.read()
    response.close()
    return link


def STRIP(name):
  return re.sub(r'\[.*?\]|\(.*?\)|\W -', ' ', name).strip()

  
def download_DB():
    import downloader
    dp = xbmcgui.DialogProgress()
    dp.create("Mikeys Karaoke","",'Building Database Please Wait', ' ')
    downloader.download(K_db, db_dir,dp)

            
def mikeyyoutube():
        addDir('[COLOR white]Search Youtube[/COLOR]','url',3,art+'Search.png','none',1)
        #if ADDON.getSetting('downloads') == 'true':
            #addDir('[COLOR '+font+']'+'D[/COLOR]ownloads','url',15,art+'Main/favorites.png','',1)
        setView('movies', 'MAIN')

            
def Next_Page(link):
    link = link.split('class="paging-bar-pages">')[1]
    link=link.split('<a href=')
    for l in link:
        match=re.compile('"(.+?)#.+?" class="arrow">&gt;</a>').findall(l)        
        if match:
            return match
    return None 


    
    
def SEARCH(url):
        PAGE=1
        keyboard = xbmc.Keyboard('', 'Search')
        keyboard.doModal()
        if keyboard.isConfirmed() and len(keyboard.getText())>0:
           TXT='https://www.youtube.com/results?search_query=%s&hl=en-GB&page='  % (keyboard.getText().replace(' ','+'))
           html=OPEN_URL(TXT+str(PAGE))
        else: return
        link=html.split('class="yt-lockup-title">')
        for p in link:
            try:
                url=p.split('watch?v=')[1]
                name= p.split('title="')[1]
                name=name.split('"')[0]  
                name = str(name).replace("&#39;","'") .replace("&amp;","and") .replace("&#252;","u") .replace("&quot;","").replace("[","").replace("]","").replace("-"," ")
                iconimage = 'http://i.ytimg.com/vi/%s/0.jpg' % url.split('"')[0]
                if not 'video_id' in name:
                    if not '_title_' in name:
                        addLink(name,url.split('"')[0] ,iconimage,'')
            except:pass
   
        addDir('[COLOR blue][B]Next Page >>[/B][/COLOR]',TXT,11,art+'nextpage.png','',PAGE)
        setView('movies', 'default')
                                                                                

            
            
def nextpage(url,number):
        URL=url
        PAGE=int(number)+1
        html=OPEN_URL(url+str(PAGE))
        link=html.split('class="yt-lockup-title">')
        for p in link:
            try:
                url=p.split('watch?v=')[1]
                name= p.split('title="')[1]
                name=name.split('"')[0]  
                name = str(name).replace("&#39;","'") .replace("&amp;","and") .replace("&#252;","u") .replace("&quot;","").replace("[","").replace("]","").replace("-"," ")
                iconimage = 'http://i.ytimg.com/vi/%s/0.jpg' % url.split('"')[0]
                if not 'video_id' in name:
                    if not '_title_' in name:
                        addLink(name,url.split('"')[0] ,iconimage,'')
            except:pass
   
        addDir('[COLOR blue][B]Next Page >>[/B][/COLOR]',URL,11,art+'nextpage.png','',PAGE)
        setView('movies', 'default')
        
            
def addFile(file):
        name = file.replace(downloads,'').replace('.mp4','')
        name = name.split('-[')[-2]
        thumb = icon(file)[0]
        iconimage = 'http://i.ytimg.com/vi/%s/0.jpg' % thumb
        url=file
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name})
        liz.setProperty("IsPlayable","true")
        liz = xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
        contextMenu = []
        contextMenu.append(('Delete', 'XBMC.RunPlugin(%s?mode=102&url=%s&iconimage=%s)'% (sys.argv[0], file,iconimage)))
        liz.addContextMenuItems(contextMenu,replaceItems=True)
        xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url=url,listitem = liz, isFolder = False)
        setView('movies', 'VIDEO')
        xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_LABEL)    

def addFileSF(file):
        iconimage = file.replace('.avi','.jpg').replace('.mp4','.jpg')
        name = file.replace(sfdownloads,'').replace('.avi','').replace('.mp4','')
        url=file
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name})
        liz.setProperty("IsPlayable","true")
        liz = xbmcgui.ListItem(name, iconImage=iconimage, thumbnailImage=iconimage)
        contextMenu = []
        contextMenu.append(('Delete', 'XBMC.RunPlugin(%s?mode=102&url=%s&iconimage=%s)'% (sys.argv[0], file,iconimage)))
        liz.addContextMenuItems(contextMenu,replaceItems=True)
        xbmcplugin.addDirectoryItem(handle = int(sys.argv[1]), url=url,listitem = liz, isFolder = False)
        setView('movies', 'VIDEO')
        xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_LABEL)   
        
                
def deleteFileSF(file,iconimage):
    tries    = 0
    maxTries = 10
    while os.path.exists(file) and tries < maxTries:
        try:
            os.remove(file)
            break
        except:
            xbmc.sleep(500)
            tries = tries + 1
    while os.path.exists(iconimage) and tries < maxTries:
        try:
            os.remove(iconimage)
            break
        except:
            xbmc.sleep(500)
            tries = tries + 1
            
            
    if os.path.exists(file):
        d = xbmcgui.Dialog()
        d.ok('Mikeys Karaoke', 'Failed to delete file')         
                           
def get_params():
        param=[]
        paramstring=sys.argv[2]
        if len(paramstring)>=2:
                params=sys.argv[2]
                cleanedparams=params.replace('?','')
                if (params[len(params)-1]=='/'):
                        params=params[0:len(params)-2]
                pairsofparams=cleanedparams.split('&')
                param={}
                for i in range(len(pairsofparams)):
                        splitparams={}
                        splitparams=pairsofparams[i].split('=')
                        if (len(splitparams))==2:
                                param[splitparams[0]]=splitparams[1]
                                
        return param
        

    

        
def addDir(name,url,mode,iconimage,fanart,number):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)+"&fanart="+urllib.quote_plus(fanart)+"&number="+str(number)
        liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png", thumbnailImage=iconimage)
        liz.setProperty( "Fanart_Image", fanart )
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        if (mode == 2000)or mode==103 or mode==203:
            if mode ==203:
                liz.setProperty("IsPlayable","true")
            xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz, isFolder=False)
        else:
            xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz, isFolder=True)
        if not mode==1 and mode==20 and mode==19:
            xbmcplugin.addSortMethod(int(sys.argv[1]), xbmcplugin.SORT_METHOD_LABEL)
            
        
def addLink(name,url,iconimage, fanart,showcontext=True):
        u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode=3001&name="+urllib.quote_plus(name)+"&iconimage="+urllib.quote_plus(iconimage)
        import yt
        liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
        liz.setInfo( type="Video", infoLabels={ "Title": name } )
        liz.setProperty("IsPlayable","true")
        #menu = []
        
        #if ADDON.getSetting('downloads') == 'true':
            #menu.append(('Download', 'XBMC.RunPlugin(%s)' % cmd))   
        #liz.addContextMenuItems(items=menu, replaceItems=True)
        ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False)
        


def PlayYouTube(name,url,iconimage):
    import yt
    youtube=yt.PlayVideo(url)    
    liz = xbmcgui.ListItem(name, iconImage='DefaultVideo.png', thumbnailImage=iconimage)
    liz.setInfo(type='Video', infoLabels={'Title':name})
    liz.setProperty("IsPlayable","true")
    liz.setPath(str(youtube))
    xbmcplugin.setResolvedUrl(int(sys.argv[1]), True, liz)
                                


      

params=get_params()
url=None
name=None
mode=None
iconimage=None
fanart=None

try:
        url=urllib.unquote_plus(params["url"])
except:
        pass
try:
        name=urllib.unquote_plus(params["name"])
except:
        pass
try:
        iconimage=urllib.unquote_plus(params["iconimage"])
except:
        pass
try:
        switch=urllib.unquote_plus(params["switch"])
except:
        switch='display'
try:        
        mode=int(params["mode"])
except:
        pass
try:        
        fanart=urllib.unquote_plus(params["fanart"])
except:
        pass
try:        
        number=int(params["number"])
except:
        pass
try:        
        split=int(params["split"])
except:
        pass
                
print "Mode: "+str(mode)
print "URL: "+str(url)
print "Name: "+str(name)
print "IconImage: "+str(iconimage)
print "FanartImage: "+str(fanart)
try:print "number: "+str(number)
except:pass

def setView(content, viewType):
    # set content type so library shows more views and info
    if content:
        xbmcplugin.setContent(int(sys.argv[1]), content)
    if ADDON.getSetting('auto-view') == 'true':
        xbmc.executebuiltin("Container.SetViewMode(%s)" % ADDON.getSetting(viewType) )
        
        
if mode==None or url==None or len(url)<1:
        mikeyyoutube()
        
elif mode==3:
        print ""+url
        SEARCH(url)        
        
elif mode==9:
        TITLE_ORDERS_YOUTUBE(name, url, fanart)   
        
                      
elif mode==11:
        nextpage(url,number)  
        
elif mode==12:
    pass
elif mode==13:
    addFavorite(name,url,iconimage,fanart)

elif mode==14:
    rmFavorite(name)
        
elif mode==15:
    DOWNLOADS(downloads)
    
elif mode==16:
    Sunflysearch(url)
    
elif mode==17:
    Sunflyurl(name)
    
elif mode==19:
    mikeyyoutube(url)


elif mode==3001:
    PlayYouTube (name,url,iconimage)
    
xbmcplugin.endOfDirectory(int(sys.argv[1]))

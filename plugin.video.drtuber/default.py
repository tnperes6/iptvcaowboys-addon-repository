import urllib,urllib2,re,os,sys
import xbmc,xbmcplugin,xbmcgui,xbmcaddon
import HTMLParser,htmllib
import hashlib #md5

homepage = 'http://www.drtuber.com'

__settings__ = xbmcaddon.Addon(id='plugin.video.drtuber')
home = __settings__.getAddonInfo('path')
imagedir = xbmc.translatePath( os.path.join( home, 'images' ) )

def unescape(s):
  p = htmllib.HTMLParser(None)
  p.save_bgn()
  p.feed(s)
  return p.save_end()

def GET_HTML(url):
  req = urllib2.Request(url)
  req.add_header('User-Agent','Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0')
  response = urllib2.urlopen(req)
  html=response.read()
  response.close()
  return html

def HOME():
  print '################### HOME ###################'
  addDir('  Categories->',homepage +'/categories/',1,'')
  LIST_ITEMS(homepage)

def LIST_SECTIONS(url):
  print '############### LIST SECTIONS ##############'
  html = GET_HTML(url)
  str_match=re.compile('<div class="title_bar2 straight">Straight</div>(.+?)<div class="title_bar2', re.DOTALL | re.MULTILINE).findall(html)
  gay_match=re.compile('<div class="title_bar2 gay">Gays</div>(.+?)<div class="title_bar2', re.DOTALL | re.MULTILINE).findall(html)
  trans_match=re.compile('<div class="title_bar2 trans">Transsexual</div>(.+?)<div class="baner_bottom">', re.DOTALL | re.MULTILINE).findall(html)

  for str_html in str_match:
    match=re.compile('<a class="btnBig" href="(.+?)">(.+?)<span>.+?\((.+?)\)</span></a>', re.DOTALL | re.MULTILINE).findall(str_html)
    addDir('---------- Straight ----------','',2,'')
    for cat_url,title,num in match:
      url = homepage + cat_url
      title = title + ' (' + num + ')'
      addDir(title,url,2,'')
  for gay_html in gay_match:
    match=re.compile('<a class="btnBig" href="(.+?)">(.+?)<span>.+?\((.+?)\)</span></a>', re.DOTALL | re.MULTILINE).findall(gay_html)
    addDir('---------- Gay ----------','',2,'')
    for cat_url,title,num in match:
      url = homepage + cat_url
      title = title + ' (' + num + ')'
      addDir(title,url,2,'')
  for trans_html in trans_match:
    match=re.compile('<a class="btnBig" href="(.+?)">(.+?)<span>.+?\((.+?)\)</span></a>', re.DOTALL | re.MULTILINE).findall(trans_html)
    addDir('---------- Transsexual ----------','',2,'')
    for cat_url,title,num in match:
      url = homepage + cat_url
      title = title + ' (' + num + ')'
      addDir(title,url,2,'')

def LIST_ITEMS(url):
  print '################ LIST_ITEMS ################'
  html = GET_HTML(url)
#  match=re.compile('<a class="img_time  ch-video".+?href="(.+?)">(.+?)id=".+?".+?src="(.+?)".+?alt="(.+?)".+?<div class="item_info">Runtime: (.+?) .+?</div>', re.DOTALL | re.MULTILINE).findall(html)
  match=re.compile('<a href="(.+?)" class="th ch-video">.+?src="(.+?)" alt="(.+?)" height(.+?)<i class="time">(.+?)</i>', re.DOTALL | re.MULTILINE).findall(html)
  for vid_url,img,title,junk,time in match:
    hd_match = re.search(r'class="hd"', junk)
    if(not hd_match):
      title = unescape(title) + '  (' + time + ')'
    else:
      title = unescape(title) + ' [COLOR red]HD[/COLOR]  (' + time + ')'
    url = homepage + vid_url
    addDownLink(title,url,3,img)

  next=re.compile('<a class="next" href="(.+?)">Nex').findall(html)
  for url in next:
    thumb = xbmc.translatePath( os.path.join( imagedir, "next.png") )
    addDir('--> Next Page',url,2,thumb)

def VIDEOLINKS(url,title):
  print '################## RESOLVE #################'
  html = GET_HTML(url)
  match=re.compile('params \+= \'h=(.+?)\'.+?\'%26t=(.+?)\'.+?\'.+?\'.+?\'(.+?)\'', re.DOTALL | re.MULTILINE).findall(html)
  for host,time,vkey in match:
    m = hashlib.md5()
    m.update(vkey + 'PT6l13umqV8K827')
    pkey = m.hexdigest()
    config = 'http://www.drtuber.com/player_config/?h=' + host + '&t=' + time + '&vkey=' + vkey + '&pkey=' + pkey + '&aid=&domain_id='
    conf_html = GET_HTML(config)
    stream=re.compile('<video_file><\!\[CDATA\[(.+?)\]\]></video_file>').findall(conf_html)
    for link in stream:
      print 'Link: ' + str(link)
      listitem = xbmcgui.ListItem(title)
      listitem.setInfo(type="video", infoLabels={ "Title": title } )
      xbmc.Player( xbmc.PLAYER_CORE_DVDPLAYER ).play(link, listitem)

def get_params():
  param=[]
  paramstring=sys.argv[2]
  if len(paramstring)>=2:
    params=sys.argv[2]
    cleanedparams=params.replace('?','')
    if (params[len(params)-1]=='/'):
      params=params[0:len(params)-2]
    pairsofparams=cleanedparams.split('&')
    param={}
    for i in range(len(pairsofparams)):
      splitparams={}
      splitparams=pairsofparams[i].split('=')
      if (len(splitparams))==2:
        param[splitparams[0]]=splitparams[1]
  return param

def addDir(name,url,mode,iconimage):
  u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
  ok=True
  liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png",thumbnailImage=iconimage)
  liz.setInfo( type="Video", infoLabels={ "Title": name })
  ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
  return ok

def addDownLink(name,url,mode,iconimage):
  u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
  ok=True
  liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
  liz.setInfo( type="Video", infoLabels={ "Title": name } )
  ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False)
  return ok

#def addLink(name,url,mode,iconimage):


params=get_params()
url=None
name=None
mode=None

try:
  url=urllib.unquote_plus(params["url"])
except:
  pass
try:
  name=urllib.unquote_plus(params["name"])
except:
  pass
try:
  mode=int(params["mode"])
except:
  pass

print 'Mode: ' + str(mode)
print 'URL : ' + str(url)
print 'Name: ' + str(name)

if mode==None or url==None or len(url)<1:
  HOME()
elif mode==1:
  LIST_SECTIONS(url)
elif mode==2:
  LIST_ITEMS(url)
elif mode==3:
  VIDEOLINKS(url,name)

xbmcplugin.endOfDirectory(int(sys.argv[1]))

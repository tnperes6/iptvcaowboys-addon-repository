__scriptname__ = "XXXYMovies.com"
__author__ = "achilles"
__url__ = "https://code.google.com/p/achilles-projects/"
__scriptid__ = "plugin.video.xxxymovies"
__credits__ = "Anyone that sees any of their code in here..."
__version__ = "1.0.0"

import urllib,urllib2,re
import xbmc,xbmcplugin,xbmcgui,sys
from BeautifulSoup import BeautifulSoup

homepage = 'http://www.xxxymovies.com'

def title_case(line):
  return ' '.join([s[0].upper() + s[1:] for s in line.split(' ')])

def GET_HTML(url):
  req = urllib2.Request(url)
  req.add_header('User-Agent','Mozilla/5.0 (Windows NT 5.1; rv:31.0) Gecko/20100101 Firefox/31.0')
  req.add_header('Referer', homepage)
  response = urllib2.urlopen(req)
  html = response.read()
  html = html.replace('<l','\n<l')
  html = html.replace('<p','\n<p')
  html = html.replace('<s','\n<s')
  html = html.replace('<i','\n<i')
  html = html.replace('<a','\n<a')
  response.close()
  return html

def HOME():
  html = GET_HTML(homepage)
  addDir('Most Recent',homepage,1,'')
  addDir('Top-Rated',homepage + '/best_videos/1.html',1,'')
  addDir('  This Week',homepage + '/best_videos/this_week/1.html',1,'')
  addDir('  This Month',homepage + '/best_videos/this_month/1.html',1,'')
  addDir('Most Viewed',homepage + '/most_viewed/1.html',1,'')
  addDir('  This Week','/most_viewed/this_week/1.html',1,'')
  addDir('  This Month','/most_viewed/this_month/1.html',1,'')
  addDir('Longest',homepage + '/longest_videos/1.html',1,'')
  addDir('  This Week','/longest_videos/this_week/1.html',1,'')
  addDir('  This Month','/longest_videos/this_month/1.html',1,'')
  addDir('Categories','',1,'')
  match = re.compile('<a id="catlink" href="(.+?)" title=".+?">(.+?)</a>', re.DOTALL | re.MULTILINE).findall(html)
  for cat_url,cat_title in match:
    cat_url = homepage + cat_url
    addDir('  ' + cat_title,cat_url,1,'')
  xbmcplugin.endOfDirectory(int(sys.argv[1]))

def LIST_ITEMS(url):
  html = GET_HTML(url)
  soup = BeautifulSoup(html)
  for item in soup.findAll('li', {'class': ['videowrap'], 'itemscope': ['itemscope']}):
    match=re.compile('<a href="(.+?)" itemprop="url">.+?<img src="(.+?)" id=".+?" class="vimg" itemprop="image" alt="(.+?)".+?<span class="linfo">(.+?)</span><meta itemprop="duration"', re.DOTALL | re.MULTILINE).findall(str(item))
    for link,img,title,time in match:
      title = title + '  (' + time + ')'
      addDownLink(title,homepage + link,2,img)
  try:
    next = soup.find('a', {'class': ['pgarrow_r']})['href']
    if '?' in url:
      pos=url.find('?')
      url=url[:pos]
#    print 'URL1: ' + url
    url = url.rstrip('/')
    addDir(' Next Page',url + '/' + next,1,'')
  except:
    pass
  xbmcplugin.endOfDirectory(int(sys.argv[1]))

def VIDEOLINKS(url,title):
  html = GET_HTML(url)
  templink = re.search('playlist: "(.+?)"', html).group(1)
  playlist = GET_HTML(templink)
  link = re.search('<jwplayer:source file="(.+?)"', playlist).group(1)
  listitem = xbmcgui.ListItem(title)
  listitem.setInfo('video', {'Title': title})
  xbmc.Player().play( str(link), listitem)

def get_params():
  param=[]
  paramstring=sys.argv[2]
  if len(paramstring)>=2:
    params=sys.argv[2]
    cleanedparams=params.replace('?','')
    if (params[len(params)-1]=='/'):
      params=params[0:len(params)-2]
    pairsofparams=cleanedparams.split('&')
    param={}
    for i in range(len(pairsofparams)):
      splitparams={}
      splitparams=pairsofparams[i].split('=')
      if (len(splitparams))==2:
        param[splitparams[0]]=splitparams[1]
  return param

def addDir(name,url,mode,iconimage):
  u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
  ok=True
  liz=xbmcgui.ListItem(name, iconImage="DefaultFolder.png",thumbnailImage=iconimage)
  liz.setInfo( type="Video", infoLabels={ "Title": name })
  ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=True)
  return ok

def addDownLink(name,url,mode,iconimage):
  u=sys.argv[0]+"?url="+urllib.quote_plus(url)+"&mode="+str(mode)+"&name="+urllib.quote_plus(name)
  ok=True
  liz=xbmcgui.ListItem(name, iconImage="DefaultVideo.png", thumbnailImage=iconimage)
  liz.setInfo( type="Video", infoLabels={ "Title": name } )

  ok=xbmcplugin.addDirectoryItem(handle=int(sys.argv[1]),url=u,listitem=liz,isFolder=False)
  return ok

params=get_params()
url=None
name=None
mode=None

try:
  url=urllib.unquote_plus(params["url"])
except:
  pass
try:
  name=urllib.unquote_plus(params["name"])
except:
  pass
try:
  mode=int(params["mode"])
except:
  pass

print 'Mode: ' + str(mode)
print 'URL : ' + str(url)
print 'Name: ' + str(name)

if mode==None or url==None or len(url)<1:
  HOME()
elif mode==1:
  LIST_ITEMS(url)
elif mode==2:
  VIDEOLINKS(url,name)

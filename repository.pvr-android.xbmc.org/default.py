import xbmcplugin,xbmcgui,xbmc,xbmcaddon,os,sys
AddonID="repository.pvr-android.xbmc.org"
AddonName="XBMC.org PVR Add-ons"
dialog=xbmcgui.Dialog()
xbmc.executebuiltin("UpdateLocalAddons")
xbmc.executebuiltin("UpdateAddonRepos")
choice=dialog.yesno(AddonName+" Add-on Requires Update","This add-on may still be in the process of the updating, would you like check the status of your add-on updates or try re-installing via the Total Installer backup method? We highly recommend checking for updates.",yeslabel="Install Option 2", nolabel="Check Updates")
if choice==0: xbmc.executebuiltin('ActivateWindow(10040,"addons://outdated/",return)')
else: xbmc.executebuiltin('ActivateWindow(10001,"plugin://plugin.program.tbs/?mode=grab_addons&url=%26redirect%26addonid%3d'+AddonID+'")')
xbmcplugin.endOfDirectory(int(sys.argv[1]))